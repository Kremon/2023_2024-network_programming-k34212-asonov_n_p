University: ITMO University

Faculty: FICT

Course: Network programming

Year: 2023/2024

Group: K34212

Author: Асонов Николай Павлович

Lab: Lab3

# Развертывание Netbox, сеть связи как источник правды в системе технического учета Netbox

**Цель:** С помощью Ansible и Netbox собрать всю возможную информацию об устройствах и сохранить их в отдельном файле.

## Ход работы

# Установка и настройка Netbox

Установлен и настроен postgreSQL с помощью команды `sudo apt install postgresql libpq-dev -y`

Далее была создана новая база данных netbox, а также user с максимальными правами на управление бд:

<img alt='image' src="images/connect to postgres.png">  

<br>
<img alt='image' src="images/create db and user.png">
Далее установлен Redis - хранилище значений ключей в памяти, которое netbox использует для кэширования и постановки в очередь: `sudo apt install -y redis-server`

<img alt='image' src="images/ping redis.png">

Установлены все нужные для дальнейшей работы пакеты:

`sudo apt install python3 python3-pip python3-venv python3-dev build-essential libxml2-dev libxslt1-dev libffi-dev libpq-dev libssl-dev zlib1g-dev git -y`

Обновлён `pip` до последней версии:

`sudo pip3 install --upgrade pip`

Создана директория, куда будет клонирован репозиторий netbox:

<img alt='image' src="images/netbox_dir.png">

Склонирован репозиторий:

<img alt='image' src="images/clone netbox.png">
Создан системный пользователь netbox с правами на все файлы:

<img alt='image' src="images/netbox_user.png">

Скопирован `configuration.example.py`, который будет использоваться для дальнейшей настройки и сгенерирован секретный ключ:

<img alt='image' src="images/make_conf_file.png">

Файл `configuration.py` отредактирован следующим образом:

<img alt='image' src="images/db_conf.png">

<img alt='image' src="images/secret_key.png">

Запущен upgrade скрипт с помощью команды `sudo /opt/netbox/upgrade.sh`

<img alt='image' src="images/upgrade_netbox.png">

Выполнен вход в виртуальную среду: `source /opt/netbox/venv/bin/activate`

Создан аккаунт супер-юзера:

<img alt='image' src="images/venv_create_user.png">

Для запуска netbox в режиме daemon были сконфигурированы nginx и gunicorn.

Настройка gunicorn:

<img alt='image' src="images/configure netbox gunicorn.png">

Настройка Nginx Web Server:
- Установка: `sudo apt install -y nginx`
- Копирование Netbox Nginx конфигурации: `sudo cp /opt/netbox/contrib/nginx.conf /etc/nginx/sites-available/netbox`
- Файл netbox отредактирован так:

  <img alt='image' src="images/nginx_conf.png">
- Удаление дефолтной конфигурации: `sudo rm /etc/nginx/sites-enabled/default`
- Перезагрузка nginx: `sudo systemctl restart nginx`

Перезапущены сервисы и добавлены в автозагрузку. NetBox и nginx работают:

<img alt='image' src="images/nginx service.png">
Теперь графический интерфейс netbox можно открыть в браузере:
<img alt='image' src="images/netbox.png">

# Создание роутеров в Netbox

Сведения о виртуальных роутерах из предыдущих работ были внесены в раздел Virtual Machines:
<img alt='image' src="images/netbox_virtual_machines.png">

# Работа с ansible 

Где-то в этом моменте отлетели ссш ключи для подключения к виртуалке и пришлось все делать заново :(

Был создан inventory файл, который получает данные о роутерах из Netbox. Подключение производилось с помощью сгенерированного в Netbox токена.

<img alt='image' src="images/inventory.png">

Далее написан файл playbook для получения данных:

<img alt='image' src="images/playbook1_text.png">

Файл выполнен корректно:

<img alt='image' src="images/1playbook.png">

Результат:

<img alt='image' src="images/routers_conf.png">


Далее написан еще один playbook, который изменяет в netbox названия виртуальных машин роутеров.

<img alt='image' src="images/playbook2_text.png">

Playbook успешно выполнился, на роутерах появился новый интерфейс, поменялось имя.

<img alt='image' src="images/2playbook.png">


<img alt='image' src="images/router_changed.png">

Далее по заданию нужно получить серийный номер устройства и внести эту информацию в Netbox. Но для вирутальных устройств серийный номер не выдается, поэтому были выбраны сведения об архитектуре устройства для занесения новой информации - необходимо получать эти данные, и потом передавать значение в переменной в кастомное поле в конфигурации роутера в netbox. 
Для этого написан playbook:

<img alt='image' src="images/playbook3_text.png">

<img alt='image' src="images/3playbook.png">

Операция выполнена успешно - в логах есть сообщения об обновлении:

ПОДСТАВИТЬ ИЗ НЕТБОКС СКРИН С РОУТЕРАМИ
<img alt='image' src="images/.png">

<img alt='image' src="images/changed.png">

## Вывод
В ходе лабораторной работы с помощью Ansible и Netbox собрана вся возможная информация об устройствах. Было выяснено на практике, что netbox довольно удобный инструмент для документирования сетей.
University: ITMO University

Faculty: FICT

Course: Network programming

Year: 2023/2024

Group: K34212

Author: Асонов Николай Павлович

Lab: Lab4


# Базовая 'коммутация' и туннелирование с использованием языка программирования P4

**Цель:** изучить синтаксис языка программирования P4 и выполнить два обучающих задания от Open network foundation для ознакомления на практике с P4.

## Ход работы

### Подготовка среды

На рабочем компьютере установлены VirtualBox и Vagrant

 Склонирован репозиторий p4lang tutorials:

<img alt="image" src='img/git clone.png'>

Далее запущен vagrant (для завершения установки пришлось подождать около часа)

После установки в VirtualBox видим виртуальную машину с двумя аккаунтами P4 и Vagrant:

<img alt="image" src='img/vagrant_open_vm.png'>

### basic

В задании нужно настроить переадресацию на switch на такой топологии:

<img alt="image" src="https://raw.githubusercontent.com/p4lang/tutorials/master/exercises/basic/pod-topo/pod-topo.png">

Топология сети описана в json:

<img alt="image" src='img/topology.png'>

Также для каждого свитча есть правила, также в формате json:

<img alt="image" src='img/s1-runtime.png'>

Запуск mininet в папке задания:

<img alt="image" src='img/make_run.png'>

Проверка несвязности устройств с помощью ping:

<img alt="image" src='img/ping1.png'>

Теперь можно перейти к исправлению файла basic.p4, где и содержится вся основная логика. Сперва определяются типы и структуры данных (HEADERS):

<img alt="image" src='img/basic.p4.png'>

Далее описан парсер, который должен переводить заголовки из входящего пакета в вышеописанную структуру.

Для этого из пакета вместе с содержимым берём заголовок *ethernet* -> смотрим, соответствует ли поле *etherType* номеру протокола *ipv4* -> если да, то берем и заголовок *ipv4*^:

<img alt="image" src='img/basic.p4.2.png'>

Далее было необходимо дописать логику для входящих пакетов и объединения их в пакет. 

Для этого совершаются проверки *ipv4* -> далее адрес назначения пакета соотносится с таблицей *ipv4_lpm* -> если совпадают, то выполняется действие, которое указано в правилах. *ipv4_forward* - это действие переадресации, которое указывает порт для выхода, изменяет mac-address источника на тот, что был указан в пакете как адрес назначения, вместе с этим меняется адрес назначения на адрес, взятый из таблицы. ttl пакета уменьшается на 1.

<img alt="image" src='img/basic.p4.3.png'>

В deparser происходит упаковка заголовков:

<img alt="image" src='img/basic.p4.4.png'>

Для проверки корректности кода снова запущен mininet. Проверка связности:

<img alt="image" src='img/ping_2.png'>

### basic tunnel

В этом задании нужно добавить заголовок инкапсуляции *ipv4*.

Топология следующая:

<img alt="image" src='https://raw.githubusercontent.com/p4lang/tutorials/master/exercises/basic_tunnel/topo.png'>

<img alt="image" src='img/topology2.png'>

Здесь необходимо добавить проверку соответствия *etherType* и *myTunnel*. Если соответствие есть, то распаковываем *myTunnel*, если *ipv4* инкапсулирвоан в *myTunnel*, то распакавываем и его:

<img alt="image" src='img/basic_tunnel1.png'>

Далее, если присутствует заголовок *myTunnel*, то обрабатываем пакеты согласно правилам, описанным в *myTunnel_exact*. Если заголовка *myTunnel* нет, но есть *ipv4*, то используем правила из *ipv4_lpm*.

Здесь также добавляется действие *myTunnel_forward*, сипользующееся для пересылки - указываем порт направления пакета из правил.

<img alt="image" src='img/basic_tunnel2.png'>

В deparser происходит упаковка заголовка *myTunnel*:

<img alt="image" src='img/basic_tunnel3.png'>

Далее проверим связность.

В mininet запущены терминалы h1 и h2:

<img alt="image" src='img/terms.png'>

В h2 запущен получатель и с h1 отправлен пакет. Пакет дошёл

<img alt="image" src='img/packages.png'>

Далее для проверки тоннеля запущен ещё один пакет с *myTunnel*. Пакет дошёл

<img alt="image" src='img/my_tunnel_package.png'>

Теперь с h1 запущен пакет, который должен прийти в h3 и через *myTunnel* прийти в h2:

<img alt="image" src='img/3package.png'>

Как видно, пакет дошёл до h2 - *ipv4* не сработал.

## Вывод

В ходе выполнения лабораторной работы были выполнены два ознакомительных задания для понимания работы языка p4 и настройки устройств в помощью него.
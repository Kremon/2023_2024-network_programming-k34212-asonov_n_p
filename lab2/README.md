University: ITMO University

Faculty: FICT

Course: Network programming

Year: 2023/2024

Group: K34212

Author: Асонов Николай Павлович

Lab: Lab2


# Развертывание дополнительного CHR, первый сценарий Ansible

**Цель:** c помощью Ansible настроить несколько сетевых устройств и собрать информацию о них. Правильно собрать файл Inventory.

## Ход работы

### Установка и настройка второго CHR

   1. Первым шагом была аналогичная первой работе установка второго CHR на VirtualBox через .vdi образ

   2. Далее для настройки еще одного VPN клиента на виртуальной машине с сервером была сгенерирована дополнительная пара ключей с помощью команды:

   ```wg genkey | sudo tee /etc/wireguard/wg0-client2-private.key | wg pubkey | sudo tee /etc/wireguard/wg0-client2-public.key```

   3. На сервере отредактирован файл конфига /etc/wireguard/wg0.conf:
   ```
      [Interface]
      Address = 10.10.10.10
      ListenPort = 51820
      PrivateKey = [приватный ключ сервера]
      
      [Peer]
      PublicKey = [публичный ключ клиента]
      AllowedIPs = 10.10.10.11/32
      
      [Peer]
      PublicKey = [публичный ключ клиента]
      AllowedIPs = 10.10.10.12/32
   ```
   4. Далее настроен VPN клиент на 2-м CHR:


   5. На сервере перезапущен Wireguard:

   ```systemctl restart wg-quick@wg0```


### Настройка CHR-ов с помощью Ansible:

   Создан файл /etc/ansible/hosts.yml, в котором указана информация о CHR1 и CHR2 - их и будем настраивать, а также общие переменные:
   
   <img alt="image" src='inventory.png'>

   Для дальнейшей настройки необходимо настроить ssh-соединения с ВМ в Yandex Cloud и роутерами. Для этого сгенерированы ключи, публичные ключи скопированы на роутеры:
   
    ```
    ssh-keygen
    ssh admin@10.10.10.10 "/file print file=mykey; file set mykey contents=\"`cat ~/.ssh/id_rsa.pub`\";/user ssh-keys import public-key-file=mykey.txt;/ip ssh set always-allow-password-login=yes"
    ssh admin@10.10.10.10 "/file print file=mykey; file set mykey contents=\"`cat ~/.ssh/id_rsa.pub`\";/user ssh-keys import public-key-file=mykey.txt;/ip ssh set always-allow-password-login=yes"
    ```
    
  CHR-ы доступны:

  <img alt="image" src='ping_routers.png'>

  Далее создан файл playbook.yaml - он будет содержать инструкции для выполнения задач на роутерах: 
  
  ```touch /etc/ansible/playbook.yml```

  По заданию нужно настроить креды для входа, NTP, OSPF с указанием Router ID, а также собрать данные по OSPF топологии и полный конфиг устройств. 
  
  Поэтому playbook.yaml должен выглядеть следующим образом:

  <img alt="image" src='playbook.png'>

  Далее запущен playbook:

  <img alt="image" src='playbook_works.png'>


### Проверка настройки роутеров CHR

Проверена работа OSPF на CHR1:

<img alt="image" src='router1_settings.png'>

И на CHR2:

<img alt="image" src='router2_settings.png'>

Далее проверена связность между роутерами:

<img alt="image" src='router_ping1.png'>

<img alt="image" src='router_ping2.png'>

### Схема связи

<img alt="image" src='lab2_scheme.png'>


### Вывод
В ходе выполнения лабораторной работы c помощью Ansible настроены 2 сетевых устройства: логин\пароль для входа, NTP, OSPF с указанием Router ID, собрана информацию о них. Правильно собран файл Inventory. Успешно установлена связь между всеми сетевыми устройствами.

